import javax.swing.JFrame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Created by Ali Dorostkar on 2016-10-03.
 * Superclass that tracks its location
 */
public class MoveTrackerFrame extends JFrame {
    private boolean isFullScreen = false;
    private GraphicsDevice device = null;

    public MoveTrackerFrame(){
        this("");
    }
    public MoveTrackerFrame(String title){
        super(title);

        // Register the device that this frame is rendered on.
        // If the frame is moved then the device is updated
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent e) {
                super.componentMoved(e);
                for(GraphicsDevice dev : GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
                    if (getBounds().getX() >= dev.getDefaultConfiguration().getBounds().getX() &&
                            getBounds().getX() < dev.getDefaultConfiguration().getBounds().getX() + dev.getDefaultConfiguration().getBounds().getWidth()) {
                        setDevice(dev);
                    }
                }
            }
        });
    }

    public GraphicsDevice getDevice() {
        return this.device;
    }

    public void setDevice(GraphicsDevice device) {
        this.device = device;
    }

    public boolean isFullScreen() {
        return this.isFullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        this.isFullScreen = fullScreen;
    }

//    public void goToPage(int page_number){}
//
//    public void nextPage(){}
//
//    public void previousPage(){}
}
