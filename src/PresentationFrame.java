import net.miginfocom.swing.MigLayout;

import javax.swing.JPanel;
import java.awt.*;

/**
 * Created by Ali Dorostkar on 2016-10-03.
 * The frame that the audience see
 */
public class PresentationFrame extends MoveTrackerFrame implements IPageChanger {
    private PdfViewPanel pf;

    public PdfViewPanel getPf() {
        return pf;
    }

    public PresentationFrame(PdfRenderer renderer){
        super("Audience View");
        pf = new PdfViewPanel(renderer);
        // Needed for the first time.
        pf.setBackground(Color.black);
        pf.setFocusable(true);
        // Put the component in a timeSlidePanel
        JPanel panel = new JPanel();
        MigLayout ml = new MigLayout("insets 0,fill");
        panel.setLayout(ml);
        panel.add(pf, "grow");
        panel.setBackground(Color.black);
        this.add(panel, BorderLayout.CENTER);
        this.setResizable(false);
    }

    public void switchDrawState(){
        pf.setDrawBlank(!pf.DrawBlank());
        repaint();
    }

    @Override
    public void goToPage(int current_page){
        pf.goToPage(current_page);
        repaint();
    }

    @Override
    public void nextPage(){
        pf.nextPage();
        repaint();
    }

    @Override
    public void previousPage(){
        pf.previousPage();
        repaint();
    }
}
