import javax.swing.*;
import java.awt.*;

/**
 * Created by Ali Dorostkar on 2016-10-02.
 * This class is a component which fills with a specific rendered pdf page
 */
public class PdfViewPanel extends JPanel implements IPageChanger {
    // the current page
    private int currentPage = 0;
    private PdfRenderer renderer = null;
    // Pass the last page of the document.
    // If this is enabled then the user of the component may
    // ask to get a page larger than the number of the pages
    private boolean passLast;
    // same as passLast but for the first page
    private boolean passFirst;
    // Draws a blank square regardless of the content of the page.
    private boolean drawBlank;

    PdfViewPanel(final String filePath){
        this(new PdfRenderer.RendererBuilder().File(filePath).build());
    }

    PdfViewPanel(final PdfRenderer renderer){
        super();
        this.renderer = renderer;
        setPassFirst(false);
        setPassLast(true);
        setDrawBlank(false);
    }

    public boolean PassLast() {
        return passLast;
    }

    public void setPassLast(boolean passLast) {
        this.passLast = passLast;
    }

    public boolean PassFirst() {
        return passFirst;
    }

    public void setPassFirst(boolean passFirst) {
        this.passFirst = passFirst;
    }

    public boolean DrawBlank() {
        return drawBlank;
    }

    public void setDrawBlank(boolean drawBlank) {
        this.drawBlank = drawBlank;
    }

    public int CurrentPage() {
        return currentPage;
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        if(currentPage < 0 || currentPage >= renderer.getNumberOfPages() || DrawBlank()){
            g2.setColor(Color.black);
            g2.fillRect(0,0,getWidth(),getHeight());
            return;
        }
        renderer.draw(g2, currentPage, getWidth(), getHeight());
    }

    public void goToPage(int current_page){
        currentPage = current_page;

        if(currentPage < 0)
            currentPage = (PassFirst()) ? -1 : 0;

        if(currentPage >= renderer.getNumberOfPages())
            currentPage = (PassLast()) ? renderer.getNumberOfPages() : renderer.getNumberOfPages() - 1;

        repaint();
    }

    public void nextPage(){
        goToPage(currentPage +1);
    }

    public void previousPage(){
        goToPage(currentPage -1);
    }

    public PdfRenderer getRenderer() {
        return renderer;
    }
}
