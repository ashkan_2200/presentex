import java.util.ArrayList;
import java.util.List;

/**
 * Created by alidorostkar on 2016-10-31.
 */
public class PageObserver implements IPageChanger {
    List<IPageChanger> subjects;

    public PageObserver(){
        subjects = new ArrayList<>();
    }

    public void attach(IPageChanger newSub){
        subjects.add(newSub);
    }

    public void detach(IPageChanger sub){
        subjects.remove(sub);
    }

    @Override
    public void goToPage(int page_number) {
        for (IPageChanger sub : subjects)
            sub.goToPage(page_number);
    }

    @Override
    public void nextPage() {
        for (IPageChanger sub : subjects)
            sub.nextPage();
    }

    @Override
    public void previousPage() {
        for (IPageChanger sub : subjects)
            sub.previousPage();
    }
}
