import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.prefs.Preferences;

/**
 * Created by Ali Dorostkar.
 * The class that manages the frames and keyboard input
 */

public class Presentex implements KeyListener{
    private PresentationFrame mainFrame;
    private SpeakerFrame speakerFrame;
    private PdfRenderer renderer;
    private PageObserver pObserver;
    public Presentex(final String filePath)
    {
        initialize(filePath);
    }

    private void initialize(String filePath) {
        if(filePath == null) return;
        // Create the pdf renderer
        renderer = new PdfRenderer.RendererBuilder().File(filePath).build();
        // Get screen dimension and see how large should the main frame be
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameDim = renderer.trim(new Dimension(screenSize.width*5/6, screenSize.height*5/6));
        Dimension speakDim = new Dimension((int)(screenSize.width*0.95), screenSize.height*5/6);

        //Create the main frame and configure
        mainFrame = new PresentationFrame(renderer);
        mainFrame.setBackground(Color.black);
        mainFrame.getContentPane().setPreferredSize(frameDim);
        mainFrame.setFocusable(true);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();

        // Create the speakers frame and configure
        speakerFrame = new SpeakerFrame(filePath);
        speakerFrame.setBackground(Color.black);
        speakerFrame.getContentPane().setPreferredSize(speakDim);
        speakerFrame.setFocusable(true);
        speakerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        speakerFrame.setHelp(getHelp());
        speakerFrame.pack();

        mainFrame.addKeyListener(this);
        speakerFrame.addKeyListener(this);

        pObserver = new PageObserver();
        pObserver.attach(mainFrame);
        pObserver.attach(speakerFrame);
    }

    public void show(){
        speakerFrame.setVisible(true);
        mainFrame.setVisible(true);
    }

    public static Presentex openDialog(){
        String filePath = getPdfPath();
        if(filePath == null)
            System.exit(0);

        return new Presentex(filePath);
    }

    private static String getPdfPath() {
        String filePath = null;
        Preferences pref = Preferences.userRoot();

        JFileChooser fd = new JFileChooser();
        fd.setCurrentDirectory(new File(pref.get("DEFAULT_PATH", "")));
        fd.setFileFilter(new FileNameExtensionFilter("*.pdf", "pdf"));
        int returnVal = fd.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
//            This is where a real application would open the file.
            filePath = fd.getSelectedFile().getAbsolutePath();

            pref.put("DEFAULT_PATH", filePath);
        }
        return filePath;
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_LEFT:
                pObserver.previousPage();
                break;
            case KeyEvent.VK_SPACE: // Fall through
            case KeyEvent.VK_RIGHT:
                pObserver.nextPage();
                break;
            case KeyEvent.VK_UP:
                pObserver.goToPage(0);
                break;
            case KeyEvent.VK_DOWN:
                int lp = mainFrame.getPf().getRenderer().getNumberOfPages()-1;
                pObserver.goToPage(lp);
                break;
            case KeyEvent.VK_F:
                flipFullScreen(mainFrame);
                break;
            case KeyEvent.VK_D:
                flipFullScreen(speakerFrame);
                break;
            case KeyEvent.VK_J:
                int pn = speakerFrame.getJumpPage("");
                pObserver.goToPage(pn);
                break;
            case KeyEvent.VK_S:
                mainFrame.switchDrawState();
                break;
            case KeyEvent.VK_T:
                if(e.isShiftDown())
                    speakerFrame.getTimer().reset();
                else
                    speakerFrame.getTimer().switchState();
                break;
            case KeyEvent.VK_O:
                initialize(getPdfPath());
                show();
                break;
            case KeyEvent.VK_Q:
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public String getHelp(){

        return new StringBuilder()
                .append("\n\n\nShortcuts:\n")
                .append("----------------------------\n")
                .append("LEFT  : Previous slide\n")
                .append("RIGHT/<space> : Next slide\n")
                .append("UP    : First slide\n")
                .append("DOWN  : Last slide\n")
                .append("S     : Show/Hide main content\n")
                .append("D     : Fullscreen presenter\n")
                .append("F     : Fullscreen main\n")
                .append("J     : Jump to page\n")
                .append("T     : Start/Stop timer\n")
                .append("<shift>+T: Reset timer\n")
                .append("O     : Open file chooser dialog\n")
                .append("Q     : quit")
                .toString();
    }

    private void flipFullScreen(MoveTrackerFrame frame){
        if (frame.getDevice().isFullScreenSupported() && !frame.isFullScreen()) {
            frame.getDevice().setFullScreenWindow(frame);
            frame.setFullScreen(true);
        }
        else{
            frame.getDevice().setFullScreenWindow(null);
            frame.setFullScreen(false);
        }
    }
}
