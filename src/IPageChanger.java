/**
 * Created by alidorostkar on 2016-10-31.
 */
public interface IPageChanger {
    public void goToPage(int page_number);

    public void nextPage();

    public void previousPage();
}
