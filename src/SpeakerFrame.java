import net.miginfocom.swing.MigLayout;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;

import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by Ali Dorostkar on 2016-10-03.
 * Speaker frame is the frame that the presenter will see
 */
public class SpeakerFrame extends MoveTrackerFrame implements IPageChanger {
    // PdfViewPanel for current and next slide
    private PdfViewPanel pc_cur, pc_nex;
    // Text area for annotation and help
    private JTextArea annotationArea, helpArea;
    // Label that updates the slide that is shown in the main screen
    private JLabel slideCount;

    private JEditorPane uriPane;

    // Label that shows the elapsed time
    private StopWatchLable timer;

    public SpeakerFrame(String filePath){
        super("Presenter window");

        // Initialize the components for slideview
        pc_cur         = initPC(filePath);
        pc_nex         = initPC(filePath);
        annotationArea = initJTextArea("Something here", Color.black, "arial", Font.BOLD, 14);
        helpArea       = initJTextArea("Help here", Color.black, "courier", Font.PLAIN, 12);
        uriPane        = initEditorPane();

        // Timer initialization
        timer          = new StopWatchLable();
        timer.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        timer.setForeground(Color.green);
        timer.setBackground(Color.black);
        timer.setFont(new Font("arial", Font.PLAIN, 40));

        slideCount = new JLabel();
        slideCount.setFont(new Font("arial", Font.PLAIN, 30));
        slideCount.setForeground(Color.green);
        JLabel spliter = new JLabel(" | ");
        spliter.setForeground(Color.green);
        spliter.setFont(new Font("arial", Font.PLAIN, 40));

        JPanel timeSlidePanel = new JPanel();
        timeSlidePanel.setBackground(Color.black);
        timeSlidePanel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        timeSlidePanel.add(slideCount);
        timeSlidePanel.add(spliter);
        timeSlidePanel.add(timer);

        JPanel mainPanel = new JPanel();

        MigLayout ml = new MigLayout("fill, insets 0, wrap 3",
                "10[45%!,fill]5[35%!,fill]10[20%!,fill]",
                "10[60%!,fill]5[40%!,fill]");

        mainPanel.setLayout(ml);
        mainPanel.add(pc_cur,         "grow");
        mainPanel.add(timeSlidePanel, "split 2, flowy, shrink");
        mainPanel.add(pc_nex,         "flowy, grow");
        mainPanel.add(helpArea,       "spany 2, grow");
        mainPanel.add(uriPane,        "gapleft 20, spanx 2, split 2, flowy, grow");
        mainPanel.add(annotationArea, "gapleft 20, spanx 2, flowy, grow");

        mainPanel.setBackground(Color.black);
        this.add(mainPanel, BorderLayout.CENTER);

        this.goToPage(0);
        this.pack();
        this.setResizable(false);
    }

    private JEditorPane initEditorPane() {
        JEditorPane pane = new JEditorPane();
        HTMLEditorKit kit = new HTMLEditorKit();
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule("a {color:white;}");
        styleSheet.addRule("a {font-family: sans-serif;font-size:11px;font-weight: bold;}");
        pane.setEditorKit(kit);
        pane.setEditable(false);
        pane.setFocusable(false);
        pane.setBackground(Color.black);
        pane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if(Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(e.getURL().toURI());
                        }
                        catch (IOException | URISyntaxException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });
        return pane;
    }

    private PdfViewPanel initPC(String filePath){
        PdfViewPanel pc = new PdfViewPanel(filePath);
        pc.setBackground(Color.black);
        pc.setFocusable(true);
        pc.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        return pc;
    }

    private JTextArea initJTextArea(String text, Color c, String fontName, int fontWeight, int fontSize){
        JTextArea area = new JTextArea(text);
        area.setBackground(c);
        area.setEnabled(false);
        area.setDisabledTextColor(Color.WHITE);
        area.setFont(new Font(fontName, fontWeight, fontSize));
        area.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        return area;
    }

    private void updateAnnotationString(int current_page) {
        annotationArea.setText("");
        StringBuffer sbf = new StringBuffer();
        uriPane.setVisible(pc_cur.getRenderer().getURIAnnotations(current_page).size() > 0);
        pc_cur.getRenderer().getURIAnnotations(current_page)
                .forEach(ann -> {
                    PDActionURI action = (PDActionURI) ((PDAnnotationLink) ann).getAction();
                    sbf.append(
                            String.format("<a href=%s>%s</a><br>",
                                    action.getURI(), action.getURI()
                            )
                    );}
                );
        uriPane.setText(sbf.toString());
        pc_cur.getRenderer().getTextAnnotations(current_page)
                .forEach(ann -> annotationArea.append(ann.getContents() + "\n"));
    }

    public void setHelp(final String help){
        helpArea.setText(help);
    }

    private void updateSlideString(int page_num) {
        slideCount.setText(Integer.toString(page_num + 1) + "/" + Integer.toString(pc_cur.getRenderer().getNumberOfPages()));
    }

    public int getJumpPage(String initial){
        String page = (String) JOptionPane.showInputDialog(this, null, "Select page number", JOptionPane.PLAIN_MESSAGE, null, null, initial);
        int page_n;
        try {
            page_n = Integer.parseInt(page)-1;
        }
        catch (NumberFormatException e){
            page_n = pc_cur.CurrentPage();
        }

        if (page_n < 0 || page_n >= pc_cur.getRenderer().getNumberOfPages())
            page_n = pc_cur.CurrentPage();

        return page_n;

    }

    @Override
    public void goToPage(int current_page){
        if(current_page < 0) current_page = 0;
        if(current_page >= pc_cur.getRenderer().getNumberOfPages())
            current_page = pc_cur.getRenderer().getNumberOfPages()-1;
        pc_nex.goToPage(current_page+1);
        pc_cur.goToPage(current_page);
        updateSlideString(pc_cur.CurrentPage());
        updateAnnotationString(current_page);
    }

    @Override
    public void nextPage(){
        if(pc_cur.CurrentPage() != pc_cur.getRenderer().getNumberOfPages() - 1) {
            pc_nex.nextPage();
            pc_cur.nextPage();
            updateSlideString(pc_cur.CurrentPage());
            updateAnnotationString(pc_cur.CurrentPage());
        }
    }

    @Override
    public void previousPage(){
        if(pc_cur.CurrentPage() != 0) {
            pc_nex.previousPage();
            pc_cur.previousPage();
            updateSlideString(pc_cur.CurrentPage());
            updateAnnotationString(pc_cur.CurrentPage());
        }
    }

    public StopWatchLable getTimer(){
        return this.timer;
    }
}
