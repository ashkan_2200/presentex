import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationText;
import org.apache.pdfbox.rendering.PDFRenderer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by Ali Dorostkar on 2016-10-03.
 * This is a proxy class to access the renderer which introduces some new methods.
 */
public class PdfRenderer {
    // Alignment of the pdf page in its component.
    // If it is centered or is in the corner.
    public enum Alignment{
        CENTER(0.5f), START(0.0f), END(1.0f);

        private final float val;
        Alignment(final float val){
            this.val = val;
        }
        public float Value(){return this.val;}
    }

    private Alignment xAlign = Alignment.CENTER;
    private Alignment yAlign = Alignment.END;
    private Dimension maxDim;
    private PDDocument document;
    private PDFRenderer renderer;

    private PdfRenderer(PDDocument _doc){
        document = _doc;
        renderer = new PDFRenderer(document);
        computeMaxPageDim();
    }

    private void computeMaxPageDim(){
        float w = 0;
        float h = 0;
        for(PDPage page : document.getPages()){
            if(w*h < page.getCropBox().getWidth()* page.getCropBox().getHeight()){
                w = page.getCropBox().getWidth();
                h = page.getCropBox().getHeight();
            }
        }
        maxDim = new Dimension((int)w, (int)h);
    }

    private float computeScale(int page_width, int page_height, int width, int height){
        return computeScale((float)page_width, (float) page_height, (float) width, (float) height);
    }

    private float computeScale(float page_width, float page_height, float width, float height){
        float sc = width/page_width;
        if(sc > height/page_height)
            sc = height/page_height;
        if(sc <= 0.0f)
            return 1.0f;
        return sc;
    }

    public Alignment getXAlign() {
        return xAlign;
    }

    public void setXAlign(Alignment xAlign) {
        this.xAlign = xAlign;
    }

    public Alignment getYAlign() {
        return yAlign;
    }

    public void setYAlign(Alignment yAlign) {
        this.yAlign = yAlign;
    }

    public int getNumberOfPages(){
        return document.getNumberOfPages();
    }

    // Get an initial dimension and return a trimmed dimension which is the size of
    // the document
    public Dimension trim(Dimension dim){
        float sc = computeScale(maxDim.width, maxDim.height, dim.width, dim.height);
        return new Dimension((int)(maxDim.width*sc), (int)(maxDim.height*sc));
    }

    public void draw(Graphics2D g, int page_number, int width, int height){
        g.setBackground(Color.black);

        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        try {
            PDRectangle rec = document.getPage(page_number).getCropBox();
            // Find the placement of the origin so that the drawing is centered.
            Dimension dim = trim(new Dimension(width, height));
            // translate the origin to that point
            g.translate((width - dim.width)*getXAlign().Value(), (height - dim.height)*getYAlign().Value());

            int pw = (int)rec.getWidth();
            int ph = (int)rec.getHeight();
            renderer.renderPageToGraphics(page_number, g, computeScale(pw, ph, width, height));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public Dimension getMaxDimension(){
        return maxDim;
    }

    public java.util.List<PDAnnotation> getAnnotations(int current_page){
        java.util.List<PDAnnotation> anns = new ArrayList<>();
        try {
            anns = document.getPage(current_page).getAnnotations();
        } catch (IOException ignored) {}
        return anns;

    }

    public java.util.List<PDAnnotation> getTextAnnotations(int current_page){
        java.util.List<PDAnnotation> anns = new ArrayList<>();
        try {
            anns = document.getPage(current_page).getAnnotations();
        } catch (IOException ignored) {}

        return anns.stream()
                .filter(ann -> ann instanceof PDAnnotationText)
                .collect(Collectors.toList());
    }

    public java.util.List<PDAnnotation> getURIAnnotations(int current_page){
        java.util.List<PDAnnotation> anns = new ArrayList<>();
        try {
            anns = document.getPage(current_page).getAnnotations();
        } catch (IOException ignored) {}

        return anns.stream()
                .filter(ann -> ann instanceof PDAnnotationLink)
                .filter(ann -> ((PDAnnotationLink) ann).getAction() instanceof PDActionURI)
                .collect(Collectors.toList());
    }

//    Builder to construct the object with needed inputs
    public static class RendererBuilder{
        private PDDocument _document = null;

        private void loadFile(final String filePath){
            try {
                this._document = PDDocument.load(new File(filePath));
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        public RendererBuilder Document(final PDDocument doc){
            this._document = doc;
            return this;
        }

        public RendererBuilder File(final String filePath){
            loadFile(filePath);
            return this;
        }

        public PdfRenderer build(){
            if(this._document == null)
                throw new NullPointerException("Document can't be null");
            return new PdfRenderer(this._document);
        }
    }
}
