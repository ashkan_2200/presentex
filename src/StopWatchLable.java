import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ali Dorostkar on 2016-10-04.
 * The JLabel that tracks time
 */
public class StopWatchLable extends JLabel implements ActionListener{
    private Timer timer;
    private long startTime = 0;
    private long elapsedTime = 0;
    public StopWatchLable(){
        super();
        timer = new Timer(100, this);
        startTime = this.now();
        elapsedTime = 0;
        start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        elapsedTime += this.now() - startTime;
        startTime = this.now();
        setText(formatInterval(elapsedTime));
    }

    public void switchState(){
        if(this.isStarted())
            this.stop();
        else
            this.start();
    }

    public void start(){
        startTime = this.now();
        timer.start();
    }

    public void stop(){
        timer.stop();
    }

    public void reset(){
        timer.restart();
        startTime = this.now();
        elapsedTime = 0;
    }

    private long now(){
        return  System.currentTimeMillis();
    }

    public static String formatInterval(final long l)
    {
        final long hr = TimeUnit.MILLISECONDS.toHours(l);
        final long min = TimeUnit.MILLISECONDS.toMinutes(l - TimeUnit.HOURS.toMillis(hr));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
        return String.format("%02d:%02d:%02d", hr, min, sec);
    }

    public boolean isStarted(){
        return timer.isRunning();
    }
}
