## Presentex

This program is for presentations that have **pdf** extension and are written using **beamer** .

The program have a main window and a presenter window which shows 

* The current slide
* The next slide
* The links and notes
* A timer
* Help notes

To put notes for the slides one can use the command below in the LaTeX document

    \usepackage{pdfcomment}
    \newcommand{\pdfnote}[1]{\marginnote{\pdfcomment[icon=note]{#1}}}
    ...
    \pdfnote{remember to say hello}
    
Any link placed inside the pdf will show up in the note window.